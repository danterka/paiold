<?php

class User {
	private $name;
	private $surname;
	private $email;
	private $password;

	public function __construct($name){
		$this->name = $name;
	}

	public function getName(){
		return $this->name;
	}
	public function setName($name){
		$this->name = $name;
	}
}

$user = new User('daniel');

echo $user->getName();

?>
